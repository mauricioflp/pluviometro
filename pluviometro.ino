#include <SD.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <SPI.h>
/*
   Comentado para compilação no Arduino (ubuntu)
   #include <String.h>
*/
#include <Wire.h> //I2C library
// usando biblioteca padrão do Arduino - RTC by Makuna
#include "RtcDS3231.h"

//#include "RTClib.h"
/*
  #define TOPICO_SUBSCRIBE "escutajonas"     //tópico MQTT de escuta
  String TOPICO_PUBLISH  = "pluvi1"  ;  //tópico MQTT de envio de informações para Broker
  #define ID_MQTT  "HomeJonas"
*/
#define D0    16
#define D3    0
#define D4    2
#define D5    14
#define D6    12
#define D7    13
#define D9    3
#define D10   1
#define CS_PIN  D8

int REEDCOUNT = 1;
const int idpluviometro = 1;
double tensaoMedia;

//Config Cartão (Json)
struct Config {
  char ID_MQTT [50];
  char TOPICO_SUBSCRIBE [64];
  char  TOPICO_PUBLISH [5];
  char BROKER_MQTT [16];
  int BROKER_PORT ;
  char SSID [50];
  char PASSWORD [50];
};

const char *filename = "/config.txt";
Config config;

// WIFI prof. Mauricio
// MQTT
/*char BROKER_MQTT = "200.129.242.4";
  int BROKER_PORT = 20049;
  char SSID = "CAP1";
  char PASSWORD = "cap2009sala90";
*/

//Variáveis e objetos globais
WiFiClient espClient; // Cria o objeto espClient
PubSubClient MQTT(espClient); // Instancia o Cliente MQTT passando o objeto espClient

//rtc
RTC_DS3231 rtc;

int segundo = 0;
int minuto = 30;
int hora = 15;
int data = 24;
int mes = 5;
int ano = 2018;

//Prototypes
void initSerial();
void initWiFi();
void initMQTT();
void cartaosd();
void pluvi ();
void reconectWiFi();
void mqtt_callback(char* topic, byte* payload, unsigned int length);
void VerificaConexoesWiFIEMQTT(void);


void setup()
{
  Serial.begin(115200);
  delay(20);
  // incluindo botão para modo debug no pluviometro
  pinMode(D3,INPUT);
  pinMode(D4, OUTPUT);
  // ligando o RTC na porta D4
  digitalWrite(D4, HIGH);
  delay(100);
  Serial.println("Verificando botão de debug");
  if ( digitalRead(D3) == HIGH )
    {
      // colocar debouce
      Serial.println("Entrando em modo de debug");
      modoDebug = true;
    }

 

  while (!Serial) continue;
  // Initialize SD library
  while (!SD.begin()) {
    Serial.println(F("Failed to initialize SD library"));
    delay(1000);
  }

  // Should load default config if run for the first time
  Serial.println(F("Loading configuration..."));
  loadConfiguration(filename, config);

  // Create configuration file
  Serial.println(F("Saving configuration..."));
  saveConfiguration(filename, config);
  // Dump config file
  Serial.println(F("Print config file..."));
  printFile(filename);
  initWiFi();
  initMQTT();
  VerificaConexoesWiFIEMQTT();
  MQTT.loop();
  //função do cartao sd
  Wire.begin(D1, D2);//4,5
  cartaosd();

  // verificando leitura da porta analógica
  Serial.print("Tensão na bateria = ");
  double tensao[3] ;
  tensao[0] = analogRead(A0);
  tensao[1] = analogRead(A0);
  tensao[2] = analogRead(A0);

  tensaoMedia = tensao[0]+tensao[1]+tensao[2];
  tensaoMedia /= 3;

  // calculando a equacao baseando que o circuito equivalente 
  // tem 150 KOhms assim 3.3 * 3 da tensão equivalente
  tensaoMedia  =  3.3 * ( tensaoMedia / 1024.0 );
  tensaoMedia  =  147.0/47.0 * tensaoMedia;
  Serial.println( tensaoMedia  );

    pluvi();
  
  //keep-alive da comunicação com broker MQTT
  ESP.deepSleep(0);
  //garante funcionamento das conexões WiFi e ao broker MQTT
}
void cartaosd() {

  Serial.print("Inicializando o cartão SD...");

  if (!SD.begin(CS_PIN)) {
    Serial.println("Falha, verifique se o cartão está presente.");
    //programa encerrrado
    return;
  }

  //se chegou aqui é porque o cartão foi inicializado corretamente
  Serial.println("Cartão inicializado.");
}

void initWiFi()
{
  delay(10);
  Serial.println("------Conexao WI-FI------");
  Serial.print("Conectando-se na rede: ");
  Serial.println(config.SSID);
  Serial.println("Aguarde");
  reconectWiFi();
}
void initMQTT()
{
  MQTT.setServer(config.BROKER_MQTT, config.BROKER_PORT);   //informa qual broker e porta deve ser conectado
  MQTT.setCallback(mqtt_callback);            //atribui função de callback (função chamada quando qualquer informação de um dos tópicos subescritos chega)
}

void mqtt_callback(char* topic, byte* payload, unsigned int length)
{
  String msg;

  //obtem a string do payload recebido
  for (int i = 0; i < length; i++)
  {
    char c = (char)payload[i];
    msg += c;
  }
}

void reconnectMQTT()
{
  while (!MQTT.connected())
  {
    Serial.print("* Tentando se conectar ao Broker MQTT: ");
    Serial.println(config.BROKER_MQTT);
    if (MQTT.connect(config.ID_MQTT))
    {
      Serial.println("Conectado com sucesso ao broker MQTT!");
      MQTT.subscribe(config.TOPICO_SUBSCRIBE);
    }
    else
    {
      Serial.println("Falha ao reconectar no broker.");
      Serial.println("Havera nova tentatica de conexao em 2s");
      delay(2000);
    }
  }
}

void reconectWiFi()
{
  //se já está conectado a rede WI-FI, nada é feito.
  //Caso contrário, são efetuadas tentativas de conexão
  if (WiFi.status() == WL_CONNECTED)
    return;

  WiFi.begin(config.SSID, config.PASSWORD); // Conecta na rede WI-FI

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
  }

  Serial.println();
  Serial.print("Conectado com sucesso na rede ");
  Serial.print(config.SSID);
  Serial.println("IP obtido: ");
  Serial.println(WiFi.localIP());
}

void VerificaConexoesWiFIEMQTT(void)
{
  if (!MQTT.connected())
    reconnectMQTT(); //se não há conexão com o Broker, a conexão é refeita

  reconectWiFi(); //se não há conexão com o WiFI, a conexão é refeita
}

void pluvi () {
  String msg;
  msg = String(REEDCOUNT);
  Serial.print("PULSO ENVIADO ");
  MQTT.publish(config.TOPICO_PUBLISH, msg.c_str());
  Serial.println("- Estado da saida  enviado ao broker!");
  delay(200);

  File dataFile = SD.open("LOG.csv", FILE_WRITE);//salvar em csv para abrir em planilhas.

  if (dataFile) {

    DateTime now = rtc.now();
    segundo = now.second();
    minuto = now.minute();
    hora = now.hour();
    data = now.day();
    mes = now.month();
    ano = now.year();

    dataFile.print(ano);
    dataFile.print('/');
    dataFile.print(mes);
    dataFile.print('/');
    dataFile.print(data);
    dataFile.print(";");
    dataFile.print(hora);
    dataFile.print(':');
    dataFile.print(minuto);
    dataFile.print(':');
    dataFile.print(segundo);
    dataFile.print(";");
    dataFile.println();
    dataFile.close();
  }

  else {
    Serial.println("Falha ao abrir o arquivo LOG.csv");
  }

}

//funções jsonConfigFile
void loadConfiguration(const char *filename, Config &config) {
  // Open file for reading
  File file = SD.open(filename);

  // Allocate the memory pool on the stack.
  // Don't forget to change the capacity to match your JSON document.
  // Use arduinojson.org/assistant to compute the capacity.
  StaticJsonBuffer<512> jsonBuffer;

  // Parse the root object
  JsonObject &root = jsonBuffer.parseObject(file);

  if (!root.success())
    Serial.println(F("Failed to read file, using default configuration"));

  // Copy values from the JsonObject to the Config
 strlcpy(config.TOPICO_PUBLISH,                   // <- destination
          root["TOPICO_PUBLISH"] | "1",  // <- source
          sizeof(config.TOPICO_PUBLISH));
          
  strlcpy(config.TOPICO_SUBSCRIBE,                   // <- destination
          root["TOPICO_SUBSCRIBE"] | "escutajonas",  // <- source
          sizeof(config.TOPICO_SUBSCRIBE));

  strlcpy(config.ID_MQTT,                   // <- destination
          root["ID_MQTT"] | "HomeJonas",  // <- source
          sizeof(config.ID_MQTT));

  strlcpy(config.BROKER_MQTT,                   // <- destination
          root["BROKER_MQTT"] | "200.129.242.4",  // <- source
          sizeof(config.BROKER_MQTT));

  config.BROKER_PORT = root["BROKER_PORT"] | 20049 ;

  strlcpy(config.SSID,                   // <- destination
          root["SSID"] | "CAP1",  // <- source
          sizeof(config.SSID));

  strlcpy(config.PASSWORD,                   // <- destination
          root["PASSWORD"] | "cap2009sala90",  // <- source
          sizeof(config.PASSWORD));



  // Close the file (File's destructor doesn't close the file)
  file.close();
}
void saveConfiguration(const char *filename, const Config &config) {
  // Delete existing file, otherwise the configuration is appended to the file
  SD.remove(filename);

  // Open file for writing
  File file = SD.open(filename, FILE_WRITE);
  if (!file) {
    Serial.println(F("Failed to create file"));
    return;
  }
  StaticJsonBuffer<256> jsonBuffer;

  // Parse the root object
  JsonObject &root = jsonBuffer.createObject();

  // Set the values
  root["ID_MQTT"] = config.ID_MQTT;
  root["TOPICO_SUBSCRIBE"] = config.TOPICO_SUBSCRIBE;
  root["TOPICO_PUBLISH"] = config.TOPICO_PUBLISH;
  root["BROKER_MQTT"] = config.BROKER_MQTT;
  root["BROKER_PORT"] = config.BROKER_PORT;
  root["SSID"] = config.SSID;
  root["PASSWORD"] = config.PASSWORD;

  // Serialize JSON to file
  if (root.printTo(file) == 0) {
    Serial.println(F("Failed to write to file"));
  }

  // Close the file (File's destructor doesn't close the file)
  file.close();
}

void printFile(const char *filename) {
  // Open file for reading
  File file = SD.open(filename);
  if (!file) {
    Serial.println(F("Failed to read file"));
    return;
  }

  // Extract each characters by one by one
  while (file.available()) {
    Serial.print((char)file.read());
  }
  Serial.println();

  // Close the file (File's destructor doesn't close the file)
  file.close();
}

void loop()
{

}

